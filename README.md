# Magento 2 Owl Carousel

Ten prosty moduł pozwala na korzystanie z OwlCarousel zgodnie ze sposobem Magento.

## How to install

**Ten moduł jest teraz dostępny za pośrednictwem * Packagist *! Nie musisz już określać repozytorium.**

Dodaj następujące wiersze do pliku composer.json
 
```
...
"require":{
    ...
    "kowal/owl-carousel":"^1.0.0"
 }
```
lub po prostu wykonaj polecenie w konsoli
```
composer require kowal/owl-carousel
```
 
Następnie wpisz następujące polecenia z katalogu głównego Magento:

```
$ composer update
$ ./bin/magento cache:disable
$ ./bin/magento module:enable Kowal_OwlCarousel
$ ./bin/magento setup:upgrade
$ ./bin/magento cache:enable
```
 
## Jak używać
 
 Stosując `data-mage-init`:
  
 ```html
 
 <div id="kowal-owl-demo" data-mage-init='{
   "OwlCarousel":{
         "autoPlay": 3000,
         "items" : 5,
         "itemsDesktop" : [1290,3],
         "itemsDesktopSmall" : [768,3]
   }
 }
 '>
     <div class="item"><img src="/image1.jpg" alt="Image"></div>
     <div class="item"><img src="/image2.jpg" alt="Image"></div>
     <div class="item"><img src="/image3.jpg" alt="Image"></div>
     <div class="item"><img src="/image4.jpg" alt="Image"></div>
     <div class="item"><img src="/image5.jpg" alt="Image"></div>
     <div class="item"><img src="/image6.jpg" alt="Image"></div>
 </div>
 ```
 
 Stosując tag `<script>`:
 
 ```html
 <div id="kowal-owl-demo">
     <div class="item"><img src="/image1.jpg" alt="Image"></div>
     <div class="item"><img src="/image2.jpg" alt="Image"></div>
     <div class="item"><img src="/image3.jpg" alt="Image"></div>
     <div class="item"><img src="/image4.jpg" alt="Image"></div>
     <div class="item"><img src="/image5.jpg" alt="Image"></div>
     <div class="item"><img src="/image6.jpg" alt="Image"></div>
 </div>
 
 <script type="text/x-magento-init">
     {
         "#kowal-owl-demo": {
             "OwlCarousel": {
                 "autoPlay": 3000,
                 "items": 5,
                 "itemsDesktop": [1290, 3],
                 "itemsDesktopSmall": [768, 3]
             }
         }
     }
 </script>
 ```
 
 
